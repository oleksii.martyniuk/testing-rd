package demo;

import demo.user.User;
import demo.user.UserDAO;
import demo.user.UserService;
import demo.user.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UserServiceUnitTest {
    @Mock // <- will create mock and assign it into annotated field, like -> userDao = Mockito.mock(UserDAO.class);
    private UserDAO userDAO;

    @InjectMocks //<- will find all fields with @Mock annotation, and put them into annotated object, like -> sut.setUserDao(userDAO);
    private UserService sut = new UserServiceImpl();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this); // <- responsible for magic around @Mock and @InjectMocks
    }

    @Test
    public void testFindUserById() {
        //GIVEN
        Long userId = 1L;
        User predefinedUser = new User("John Doe");
        Mockito.when(userDAO.findById(userId)).thenReturn(predefinedUser);

        //WHEN
        User result = sut.findUserById(userId);

        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(predefinedUser, result);
        Mockito.verify(userDAO, Mockito.times(1)).findById(userId);
        Mockito.verifyNoMoreInteractions(userDAO);
    }
}