package demo;

import demo.staticrandom.RandomGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RandomGenerator.class})
public class PowerMockDemo {

    @Test
    public void test() {
        PowerMockito.mockStatic(RandomGenerator.class);
        Mockito.when(RandomGenerator.veryRandom(
                Mockito.anyDouble(),
                Mockito.anyDouble()))
                .thenReturn(100500d);

        RandomGenerator.veryRandom(0, 10);

        PowerMockito.verifyStatic(Math.class);
        RandomGenerator.veryRandom(0, 10);
    }
}