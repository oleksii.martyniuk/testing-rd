package demo;

import org.junit.Test;
import org.mockito.MockSettings;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class MockitoDemo {

    @Test
    public void setup() {
        //mock creation
        List mock = Mockito.mock(ArrayList.class);
        List notMock = new ArrayList();

        //mock setup
        Mockito.when(mock.get(0)).thenReturn("my predefined value");
        Mockito.when(mock.get(1)).thenThrow(ArrayIndexOutOfBoundsException.class);

        //actual usage of mock
        mock.add("some value");
        mock.clear();

        //verification that mock was used
        Mockito.verify(mock, Mockito.times(1)).add("some value");
        Mockito.verify(mock, Mockito.times(1)).clear();
        Mockito.verify(mock, Mockito.never()).remove("some value");


        //creation of spy - wrapper around real object, which can be used for verification
        MockSettings mockConfig = Mockito.withSettings().useConstructor().defaultAnswer(Mockito.CALLS_REAL_METHODS);

        ArrayList<String> mockedArrayList = Mockito.<ArrayList>mock(ArrayList.class, mockConfig);

        //real methods are invoked
        mockedArrayList.add("something");
        mockedArrayList.remove("something");

        Mockito.verify(mockedArrayList).add("something");
        Mockito.verify(mockedArrayList).remove("something");
    }
}
