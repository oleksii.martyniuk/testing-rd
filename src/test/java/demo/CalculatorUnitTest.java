package demo;

import demo.calc.Calculator;
import org.junit.*;

public class CalculatorUnitTest {

    private Calculator calculator = new Calculator();

    @Test
    public void testAdd() {
        //GIVEN
        Double expected = 5d;

        //WHEN
        Double result = calculator.add(2, 3);

        //THEN
        Assert.assertNotNull(result);
        Assert.assertEquals("2 + 3 should be 5", expected, result);
    }

    @Test
    public void testDivide() {
        //GIVEN
        Double expected = 4d;

        //WHEN
        Double result = calculator.divide(20, 5);

        //THEN
        Assert.assertNotNull("Result cannot be null", result);
        Assert.assertEquals("20 / 5 should be 4", expected, result);
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        //WHEN
        calculator.divide(0, 0);
    }

    @Test
    public void testSubtract() {
        //GIVEN
        Double expected = 7d;

        //WHEN
        Double result = calculator.subtract(10, 3);

        //THEN
        Assert.assertNotNull("Result cannot be null", result);
        Assert.assertEquals("10 - 3 should be 7", expected, result);
    }

    @Test
    public void testMultiply() {
        //GIVEN
        Double expected = 15d;

        //WHEN
        Double result = calculator.multiply(3, 5);

        //THEN
        Assert.assertNotNull("Result cannot be null", result);
        Assert.assertEquals("3 * 5 should be 15", expected, result);
    }
}
