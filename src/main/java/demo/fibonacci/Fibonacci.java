package demo.fibonacci;

public class Fibonacci {
    public static int compute(int n) {
        if (n <= 1) {
            return n;
        }
        return Fibonacci.compute(n - 1) + Fibonacci.compute(n - 2);
    }
}