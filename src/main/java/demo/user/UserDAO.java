package demo.user;

public interface UserDAO {
    User findById(Long id);
}
