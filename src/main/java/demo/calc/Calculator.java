package demo.calc;

public class Calculator {
    public double add(int first, int second) {
        return first + second;
    }

    public double subtract(int first, int second) {
        return first - second;
    }

    public double divide(int first, int second) {
        return first / second;
    }

    public double multiply(int first, int second) {
        return first * second;
    }
}
