package demo;

import demo.fibonacci.Fibonacci;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class FibonacciTest {
    private int n;
    private int expected;

    public FibonacciTest(int n, int expected) {
        this.n = n;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "test #{index}")
    public static Object[][] data() {
        return new Object[][]{
                {0, 0}, {1, 1}, {2, 1},
                {3, 2}, {4, 3}, {5, 5},
                {6, 8}, {26, 121393}
        };
    }

    @Test
    public void testFibonacci() {
        //WHEN
        int actual = Fibonacci.compute(n);

        //THEN
        Assert.assertEquals(expected, actual);
    }
}
