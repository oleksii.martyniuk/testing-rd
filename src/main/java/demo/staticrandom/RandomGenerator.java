package demo.staticrandom;

import java.util.concurrent.ThreadLocalRandom;

public final class RandomGenerator {
    public final static double veryRandom(double origin, double bound) {
        double one = ThreadLocalRandom.current().nextDouble(origin, bound);
        double two = ThreadLocalRandom.current().nextDouble(origin, bound);

        return one * two / System.nanoTime();
    }
}