package demo.user;

public interface UserService {
    User findUserById(Long id);
}
