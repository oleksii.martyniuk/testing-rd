package demo.user;

import java.util.Objects;

public class UserServiceImpl implements UserService {
    private UserDAO userDAO;

    @Override
    public User findUserById(Long id) {
        Objects.requireNonNull(id, "id should not be NULL");
        return userDAO.findById(id);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}